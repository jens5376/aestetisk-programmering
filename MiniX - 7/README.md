# Mini x - 7

[Open source](https://gitlab.com/jens5376/aestetisk-programmering/-/blob/main/MiniX%20-%207/minix7sketch.js)

[Kørende kode](https://jens5376.gitlab.io/aestetisk-programmering/MiniX%20-%207/index.html)

![](smiley.png)

**Hvilken Mini X har du valgt at genbesøge?**

Man kan vel sige, at jeg har taget udgangspunkt i Mini X 2, som omhandlet geometri og "emojis", men jeg har også gjort brug af ideer og syntakser, som vi har lært om i Mini X 5, som omhandlet generativ kode. Programmet jeg har skabt, er en emoji-generator som skaber en ny emoji hver gang du trykker på knappen. 

**Hvilket ændriger har du lavet og hvorfor?**

Jeg har har taget udgangspunkt i den ide jeg havde i Mini X 1, men jeg har ikke taget koden og ændret i den - jeg har derimod lavet en helt ny kode ud fra mine ideer fra mini x 1. Min originale mini X 1, er to emojis, hhv. en klassisk gul smilende en og en gennemsigtig en, der kontinuerligt skifter mellem 0-1 som hoved og øjne. Min ide med det dengang, var at 0-1'er skulle repræsentere variabler så man kunne læse hvad end man ville, ind i emojin. I min nye kode, har jeg også valgt at tage udgangpunkt i at variablerne skal definere emojin, men jeg har gjort det udfra et mantra om generativ kode. 

Det første jeg gjorde, var at definere en masse variabler til de forskellige features på emojien. 

```
let headSize
let eyeSize
let eyeWidth
let pupilSize
let mouthSize
let r
let g
let b
let newFace

```

Efter jeg havde defineret mine variabler, begyndte jeg med det første jeg vidste jeg ville få brug for - nemlig en knap der kunne refresh siden, såvel at brugeren af programmet bare kan trykke på knappen for at få en ny emoji. Her valgte jeg at kalde variablen `newFace`. Jeg skabte knappen i `setup()` funktionen, ved at bruge syntaksen `newFace = createButton('new face');` efterfølgende placerede jeg knappen med `newFace.position(windowWidth/2, windowHeight/2 + 300);` så jeg sørgede for at knappen blev placeret under emojien. Så sørgede jeg for, at knappen fik en reel funktion ved første at navngive den med `newFace.mousePressed(refreshSite);`. Slutteligt lavede jeg en funktion, der genindlæste sitet når knappen blev trykket på 
```
function refreshSite(){
  window.location.reload();
}
```

Herfra begyndte jeg at kode, hvordan emojien kunne ændre sig, når siden blev genindlæst. Først forsøgte jeg mig at bruge `random()` i de forskellige værdier under `draw()`, men det virkede selvfølgeligt ikke, da `draw()` loop, hvilket gjorde at emojien begyndte at flimer. Herefter kom jeg i tanke om, at jeg skal bruge den i `setup()`, da koden heri kun bliver kørt en enkelt gang. Derfor begyndte jeg at definere mine `random()` værdier, for hver og en af dem jeg skulle bruge. 

```
    headSize = random(400, 500);
    eyeSize = random(30, 60);
    pupilSize = eyeSize/3;
    r = random(0, 255);
    g = random(0, 255);
    b = random(0, 255);
```
Jeg sørgede for, at omend at de fleste størrelser var tilfældige, skulle de aldrig komme udover en størrelse, hvor at emojien ville komme ud af propotioner - med det mener jeg, at det ene øje, ikke skulle blive større en hoved og på den måde poppe ud over hoveds optegning. Jeg sørgede også for at hhv. red, green og blue fik værd deres værdi. Jeg kunne ikke forstå at det altid blev en grålig farve, når jeg bare defineret `color`, men så gik det op for mig, at den så bare ville vælge samme værdi til hver af de tre farver. Jeg sluttede med at tegne emojien med mine forskellige værdier. 

```
  function draw() {

    //head
    fill(r, g, b);
    strokeWeight(10);
    stroke(0);
    circle(windowWidth/2, windowHeight/2, headSize);

    //eyes
    fill(255);
    strokeWeight(10);
    circle(windowWidth/2 - eyeWidth, windowHeight/2 - 70, eyeSize);
    circle(windowWidth/2 + eyeWidth, windowHeight/2 - 70, eyeSize);

    //mouth
    fill(255);
    strokeWeight(10);
    ellipse(windowWidth/2, windowHeight/2 + eyeWidth, 200, 30);
```
Kort sagt, vælger programmet en random værdi, hver gang siden bliver refreshed, hvilket gør at der bliver dannet en ny emoji udfra de forskellige variabler jeg har sat og på den måde vil der matematisk næsten være uendelig emojis. Det var grundlæggende samme tankegang jeg havde med min Mini X 1, men eksekveringen af tankegangen, var en del anderledes. 

**Hvad har du lært i denne miniX? Hvordan kunne du inkorporere eller videreudvikle koncepterne i din ReadMe/RunMe baseret på litteraturen vi har haft indtil nu?**

Jeg synes, at noget af det spændende er, at jeg har haft tanker om generativ kodning tilbage da vi lavede Mini X 1, men der havde jeg ingen ide om hvad jeg skulle kalde det eller hvad det betød. Min tankegang dengang var at fordi at koden "tilfældigt" valgte enten 0 eller 1, at der så på den måde kunne siges, at der blev dannet en ny emoji hver gang. Generativ kode handler om, at koden bliver koden på en måde, så der kommer et resultat der ikke er kontrolleret på samme måde, som normal kodning. 

**Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere dit værk perspektiver i æstetisk programmeing (henvend evt. til forordet i grundbogen)?**

Jeg synes det er blevet meget mere påfaldne over de sidste tyve år, hvor stor relation der er mellem den æstetiske del af programmering og den digitale kultur og de personer og tanker, der lægger bag. Hvis bare vi ser på emojis som koncept, har udviklingen af deres udtryk næsten 1*1 fulgt samfundets syn på indkludering og politisk korrekthed. Med det mener jeg, at vi for bare 15 år siden nok ikke ville skænke det en tanke, hvis en smiley var gul, hvor vi idag sørger for at de har flere forskellige hudfarver for at være så indkluderende som muligt. Det er et mikro kosmos, men jeg tror det afspejler ret godt, hvordan kodning og dets udvikling følger tidenstegn. 


