let headSize
let eyeSize
let eyeWidth
let pupilSize
let mouthSize
let r
let g
let b
let newFace

function setup() {
    createCanvas(windowWidth, windowHeight);
    newFace = createButton('new face');
    newFace.position(windowWidth/2, windowHeight/2 + 300);
    newFace.mousePressed(refreshSite);
    headSize = random(400, 500);
    eyeSize = random(30, 60);
    pupilSize = eyeSize/3;
    r = random(0, 255);
    g = random(0, 255);
    b = random(0, 255);
    eyeWidth = random(40, 80);

  }
  
  function draw() {

    //head
    fill(r, g, b);
    strokeWeight(10);
    stroke(0);
    circle(windowWidth/2, windowHeight/2, headSize);

    //eyes
    fill(255);
    strokeWeight(10);
    circle(windowWidth/2 - eyeWidth, windowHeight/2 - 70, eyeSize);
    circle(windowWidth/2 + eyeWidth, windowHeight/2 - 70, eyeSize);

    //mouth
    fill(255);
    strokeWeight(10);
    ellipse(windowWidth/2, windowHeight/2 + eyeWidth, 200, 30);



  }

  function refreshSite(){
    window.location.reload();
  }