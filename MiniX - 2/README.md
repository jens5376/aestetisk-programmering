# Mini X - 2

**Describe your program and what you have used and learnt**

[Kørende kode](https://jens5376.gitlab.io/aestetisk-programmering/MiniX%20-%202/index.html)

[Open source](https://gitlab.com/jens5376/aestetisk-programmering/-/blob/main/MiniX%20-%202/minix2sketch.js)

Jeg havde en intention og så har jeg det reelle udfald. Det kræver at jeg lige forklare min oprindelige ide da min nuværende kode er meget simpel. Den opringelige ide var at lave en Emoji, dannet af 5 tegn som mund og to som øjne. Disse tegn, skulle vilkårligt skifte mellem 0 og 1, altså binær kode, uafhængigt af hinanden. Jeg kunne ikke få den tegnene til at skifte med forskellig interval, desværre. 

![](smiley.png)

Den vigtiste del af min kode er sådan er den del, der konstant får tallene til at skifte mellem 0 og 1.

`let X = floor(random(2));` 

Denne linje med kode, beskriver at X gerne må komme i nærheden af 2, men aldrig op på 2 og derfor skifter den mellem 0 og 1. 

Herfra var det egentligt relativt simpelt, jeg skabte en cirkel med `circle(250,250,375);` og plottede min X ind, forskellige steder i koordinatsystemet, som set nedenfor.
```

  text(`${X}`, 175, 175);

  text(`${X}`, 325, 175);
  
  //Her tegner jeg munden
  text(`${X}`, 250, 325);
  text(`${X}`, 200, 325);
  text(`${X}`, 150, 325);
  text(`${X}`, 300, 325);
  text(`${X}`, 350, 325);
  ```

  Det eneste problem jeg reeltset stødte på herfra, var teksten pr. automatik køre mod højre og jeg derfor blev nød til at kode at teksten skulle centreres `textAlign(CENTER, CENTER);` som sikre at man ikke skriver højre modt venstre. men at det bare forbliver med midten af det koordinat jeg har givet. 

**How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on? (Try to think through the assigned reading and your coding process, and then expand that to your own experience and thoughts - this is a difficult task, you may need to spend some time thinking about it)**

Pointen med den Emojie jeg egentligt havde håbet på at kunne lave, var at der et hav af forskellige muligheder for hvordan 0'erne og 1'erne kunne stå og at man på den måde kunne læse det ind i Emojien som man har lyst til. Det der ligger til grunde for hele debatten handler jo i sidste ende om repræsentation og diversitet i udvalget af Emojies - her tænkte jeg, at en nem løsning på dette var at man som forbruger bare tillægger Emojien det man har lyst til. På Rudolf Steiner skoler har lejetøjet f.eks. ikke noget ansigt, af den grund at barnet selv skal bruge fantasien til at tillægge legetøjet personlighed og udtryk. Samme pædagogiske tilgang kunne man vel nemt bruge for Emojies? Selvfølgeligt har jeg forståelse for, at det fuldstændigt negligerer eksistensgrundlaget for Emojies, da de nu betyder hvad man vil have dem til, men man undgår også problematikken med diversitet. 

Noget der dog er meget i iøjnefaldende i debatten, er at ideen om hvordan noget så simpelt som en emoji skal se ud, på sin vis har fulgt tiden og vores nuværende idealer. Inden for historien, findes der en kategori der hedder ”kontra faktisk”, som består i at spørge hvad-nu-hvis. Jeg synes det kunne være interessant at se de teknologiske fremskridt vi har i dag, have været der i f.eks. 40’er eller 50’er. Hvis de havde lavet emojies på de tidspunkter, hvor tidsånden var en anden, havde deres emojis nok også været betydeligt mindre inkluderende, hvis ikke bare direkte racistiske. Det er  for mig at se det spændende ved æstetisk programmering – altså at æstetik er noget der udvikler sig med samfundet og kulturen den er dannet i og vi derfor har debatten om inklusion i emojis i dag. 

Foruden det, tror jeg også at der er noget magisk ved at en fuktion kan sættes til `random()`. Ret beset er vi ikke langt i vores programmeringsuddannelse, og ved ikke så meget om det, men jeg tænker at `random()` vel pr. definition upartisk? Hvis man nu sætter farven på ægte Emojies til at koden selv vælger en farve, fra hele farveskalaen, så er der vel ikke flere problemer? Jeg har derudover haft mine overvejelser med hvordan jeg skulle løse denne Mini-X, da min originale ide var at gå kontra og skabe nogle racistiske emojies, for på den måde at sætte hele debatten lidt på spidsen. Jeg kom heldigvis på bedre tanker. Hele dilemmaet virker der ikke endnu ikke til at være en løsning på, men sådan er tingene desværre til tider. 
