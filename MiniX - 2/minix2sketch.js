function setup() {
  createCanvas(1000, 500);
  textSize(40);
}

function draw() {
  background(220);

  //første smiley
  push();
  fill(255, 255, 0)
  circle(750,250,375);
  fill(255);
  circle(675, 175, 50);
  circle(825, 175, 50);
  fill(0)
  circle(665, 175, 25);
  circle(815, 175, 25);
  fill(255);
  arc(750, 300, 250, 200, 0, PI, CHORD);
  pop();
 
  //anden smiley
  circle(250,250,375);

  

  // Her sørger jeg for, at den min tekst konstant skifter mellem 0 og 1.
  let X = floor(random(2));

  //Her sikre jeg mig, at min tekst altid er i midten af de koordinator jeg sætter inden og ikke går mod højre. 
  textAlign(CENTER,CENTER);

  //Her tegner jeg de to øjne

  text(`${X}`, 175, 175);

  text(`${X}`, 325, 175);
  
  //Her tegner jeg munden
  text(`${X}`, 250, 325);
  text(`${X}`, 200, 325);
  text(`${X}`, 150, 325);
  text(`${X}`, 300, 325);
  text(`${X}`, 350, 325);


}








