# Mini X - 1

![](kode.png)

[Link til opensource](https://gitlab.com/jens5376/aestetisk-programmering/-/blob/main/MiniX%20-%201/testsketch.js)


[Link til kørende kode](https://jens5376.gitlab.io/aestetisk-programmering/MiniX%20-%201/index.html)

**What have you produced?**

I korte træk, har jeg produceret et billede af mig selv med mini x - 1 cirkulerende rundt om mit hoved. Så det første jeg har gjort, var selvfølgeligt at danne et kanvas med `function setup()` efterfulgt af `createCanvas(500,500)` for at have et sted starte. Efterfølgende fandt jeg ud af, at hvis jeg ville bruge et billede som baggrund, skulle jeg sltte det ind før `function setup()`. Jeg satte jpeg. filen, altså billedet, ind i samme mappe som jeg programmered i, herefter anvendte jeg følgende 
```
let
function preload() {
  img = loadImage("./jenner.jpeg");

}
```
Jeg må ærligt erkende at jeg ikke helt forstår hvad jeg gjorde, men heldigvis er internettet virkeligt ens ven, når det kommer til kodning. 

Nu hvor jeg havde fået mit billede ind i koden, var det sådan set bare at bruge `function(draw)` efterfulgt af `background(img)` så bagrunden referere til det billede jeg har tideligere oppe i koden. 

Mit næste mål var derefter at få skrift til at kører rundt i en cirkel. Teksten var reeltset meget simpel, der valgte jeg bare størrelse, skrifttype, farve etc. på følgende måde:
```
textSize(30);
 textFont("Castellar");
 textAlign(CENTER,CENTER);
 fill(255,0,0);
 ```
Den kringlet del var derefter at få teksten til kontinuerligt at bevæge sig. Her fandt jeg det igen på nettet, men skrællet en del af det jeg fandt væk for at få det til at passe med min vision. I kan se den fulde kode jeg brugte, i referencen nederst på siden. Færdige linje kode lyder `text('MINI - X 1',250 * sin(frameCount / (70)) + width / 2, 250 * cos(frameCount / (70)) + height / 2);` jeg vælger hvad teksten skal lyde i første del af linjen og placeringen er så der det blev lidt kompliceret. De to gange i koden der står 250, er fordi teksten selvfølgeligt skulle have omdrejningspunkt i midten af lærredet. Framcount er med til at bestemme hvor hurtigt den skal jører omkring.

Det var i det store hele det jeg har produceret. 

**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**

Selvom det er en meget primitivt og simpel ting jeg har lavet, skal man virkeligt holde tungen i munden for at forstå hver linje kode man skriver. Hvis jeg bare starter med hvor kringlet det var at sætte et billede ind. Først og fremmest skulle der laves en funktion i programmet for at få koden til at forstå den skulle bruge billedet, hvilket jeg ikke helt er sikker på hvad betyder. Derefter skulle der produceres et lærred, det var i og for sig ikke så svært men noget man normalt ikke tænker over. Slutteligt skulle jeg så sørge for at baggrunden på lærredet skulle være billedet, hvilket betød at der var en masse arbejde der ligesom blev bundet sammen da det lod sig falde på plads. Jeg tror måske jeg blev lidt overrasket over hvor meget arbejde der ligger i kodning. I hverdagen er man blevet vant til bare at trække billeder hen hvor man skal bruge dem og så forgår kodningen så subtilt i baggrunden, at man ikke regner med at denne simple ting kræver tre forskellige brudstykker. Men med det sagt, var det også en lærerig opdagelse, fordi jeg fik et meget bedre indblik i hvad det betyder at en linje koder oftest skal relatere sig til en anden linje og først da går det op i en højere enhed. Billedet jeg satte ind relateret sig til baggrunden og baggrunden til det lærred jeg havde skabt. Foruden det var det også ret overraskende hvor små fejl der skal til, for at få det hele til at gå galt. Da jeg skulle sætte mit billede ind, tog det mig godt og vel 10-15 minutter af finde ud af hvorfor det ikke kom frem, når jeg forsøgte at gå live med min kode. At et lille bogstav, hvor der skulle være et stort et, kan ødelægge hele koden er en lærestreg jeg sent vil glemme. 

**How is the coding process different from, or similar to, reading and writing text?**
Jeg vil mene at kodning rent grammatiske minder om en blanding mellem matematik og reelt retskrivning. Skrivningen når man skal fortælle programmet hvad der skal relatere sig til hvad, men matematikken til at beskrive præcis retning placering og eventuel bevægelse. Reelt set er det første gang jeg nogensinde har brugt matematik til noget nyttigt. Cos, Sin og lignede har jeg ikke forbrugt mig af siden folkeskolen og endeligt har fundet meningen med at vi skulle bruge det. Hvad der dog er meget forskellige, er hele syntaksdelen og hvordan man skal skrive de forskellige tegn. Klammer, parenteser og citationstegn fungerer meget forskellige mening og forståelse i kodning end det har i almensprog. Helt sikker noget man skal vænne sig til og lære hen af vejen, men lige nu er jeg ret forvirret over det og hvad tegnene egentligt betyder på et dybere plan, da jeg umiddelbart kun har et meget overfladisk forståelse for syntaksen. 

**What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?**
Det betyder ikke som sådan noget specifikt for mig endnu, udover at jeg ligesom prøver at udforske feltet – det hele er trods alt meget nyt. Det har dog givet mig indsigt i, hvor mange linjer kode der reelt ligger bag min dagligdag. Hvis jeg skal bruge så meget hjerneaktivitet på at få tekst til at kører rundt i en cirkel og at sætte et billede ind, kan jeg knap forstille mig hvor meget kode der må ligge bag services som sociale medier, muliplayerspil og dets lige. Det er også nemt at få blod på tanden da der, vitterligt, er uanede muligheder. Samtidigt er det svært ikke til tider at blive lidt intimideret af selv samme grund, da der er igen er så mange muligheder. Hvad angår læsningen er det en god måde at kontekstualisere hele kodningsemnet. Jeg synes dog at mange af teksterne er meget ensformige og ofte summer omkring de samme emner langtid ad gangen uden rigtigt at komme nogle vegne. 

**Referencer**

[Cikulær bevægelse](https://p5js.org/reference/#/p5/saveGif)

[Indsæt billede](https://p5js.org/reference/#/p5/preload)



