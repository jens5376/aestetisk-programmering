# Mini X - 4

[Open source](https://gitlab.com/jens5376/aestetisk-programmering/-/blob/main/MiniX%20-%204/minix4sketch.js)

[Kørende kode](https://jens5376.gitlab.io/aestetisk-programmering/MiniX%20-%204/index.html)

![](McLovin.png)

**Provide a title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival**


Jeg har valgt at tage inspiration fra en af mine yndlingsfilm – Super Bad. For at være helt nøjagtig, har jeg nærmest tyvstjålet et af de komiske elementer i filmen, nemlig det falske ID, som protagonisterne får fat fingrene i, i forhåbningen om at på den vis at kunne skaffe alkohol til en helt fest senere i filmen. Men, hvorfor har jeg så valgt at efterligne dette, når emnet omhandler ”capture-all”? Jeg har valgt det falske identifikations, fordi jeg synes det er en af de første og originale måder at modsige sig det etableret system. I videoen med Shoshana Zuboff fortalte professoren om hvordan de store konglomerater så som Facebook, Google og Amazon har gjort at data er den mest værdifulde råvare på jorden – hvilket i sig selv jo er morsomt, i og med at data ikke kan være råt, det skal altid bruges til noget specifikt. I 2006, da filmen kom ud, var selskaber som de førnævnte i deres spæde start og informationen (eller dataen) protagonisterne skal ændre, er deres fødselsdato. I 2023 er det ikke helt så simpelt. Der er samlet så meget information om os, at hvis vi skal skabe en ny identitet på internettet, er det så godt som umuligt – ”de”, hvem end ”de” er, kender os bedre end vi kender os selv. 


**Describe your program and what you have used and learnt**


I koden, har jeg gjort lidt brug af html, men kom hurtigt til konklusionen om, at hvis det skrift sjældent kan betale sig at gøre i html.format, i hvertfald i dette tilfælde, hvor jeg har gjort meget brug af tekst som skulle forblive i kanvaset. Det første jeg gjorde, var at identificere et par forskellige variabler, heraf er den vigtigste `let names = ['McLovin', 'Jørgensen', 'Andersen', 'Møller', 'Graa', 'Graasten'];`Grunden til at denne er den vigtigste, er at jeg i denne variablen gør brug af en array, altså i dette tilfælde en liste med efternavne, for at deklarerer hvilke navne der skulle kunne vælges når man trykker på Randomize knappen. Grunden til at der kun er efternavne skyldes reelt set to ting; hhv. at ID'et i filmen udelukkende har et efternavn og fordi jeg havde svært ved få knappen til at gøre to ting samtidigt. Efter jeg havde deklareret mine variabler såvel som navnene, var opgaven at så at danne en knap som var koblet dertil. Under `funtion setup()` brugte jeg DOM elementerne:

```
button = createButton('RANDOMIZE NAME');
button.position(50, 370);
button.mousePressed(randomName);
displayText = createP('');
displayText.position(135, 212);
displayText.style('font-size', '20px');
```

I disse linjer kode, bruger jeg DOM elementer til at hhv. danne en knap, postitionere den og gav den et funktionskald som jeg vil komme ind på senere. Derefter gav jeg variablen `displayText` funktionskaldet `createP('')`som er endnu et DOM element, der bruges til at fremkalde tekst i HTML og altså ikke i kanvas. Det gav efterfølgende også lidt bøvl med positionering. Slutteligt kombineret jeg knappen, arrays og skrifts funktionskaldet, da jeg defineret hvad knappen skulle gøre på følgende måde:

```
function randomName() {
let index = floor(random(names.length));
let name = names[index];
displayText.html(name);
```
koden vælger altså et navn i arrayen, når jeg trykker på knappen hvorefter `displayText = createP('')` søger for at fremkalde det valgte navn. Jeg har kørt i gennem ret mange iterationer af hvad der teknisk set er det samme, hvilket gav et godt indblik i hvordan DOM elementer fungerer.

Det letteste DOM element at bruge var `capture = createCapture(VIDEO);`som jeg brugte som billede på ID'et. Jeg sørgede for at koden gjorde brug af webcam, hvorefter jeg brugte optagelsen som billede i kanvaset på følgende måde `background(219, 215, 215);
image(capture, 15, 15, 150, 150 * capture.height / capture.width);` under draw elementerne. 


**Articulate how your program and thinking address the theme of “capture all.”**


Først og fremmest gør den brug af "overvågning" i form af at den bruger webcamet på computeren, og derigennem indhenter data gennem computeren. Selve pointen med at det skulle lige et ID var at identifikation er en af de første måder for civilizationen at tilegne individer informationer som skulle kunne bruges til noget specifikt. ID'et er ikke længerer kun noget vi har i form af papirer og kort, men noget der er blevet så digitaliseret at vi kan tilgå det med ansigtsgenkendelse og / eller fingeraftryk. Jeg synes det var spændende at udforske hvad ID efterhånden indebærer og om man overhovedet kan forfalske det på samme måde længerer. Kan man f.eks. lave et kørerkort i Photoshop der ligner det man har på telefonen og på den måde købe alkohol som 16 årig? Det ville hverfald have hjulpet drengene i Super Bad. 


**What are the cultural implications of data capture?**


Som Shoshana Zuboff også sagde i YouTube videoen, er det en misforståelse at tro at det kun er med til at skabe "skræddersyet" reklame på sociale medier. Hvis alt kan dataficeres og data bliver mere og mere værd, kan der måske blive en økonomi i alt. Indtil viderer er det det store firmaer der forhandler med deres brugeres data, men hvad hvis det var brugerne selv det kunne vælger at sælge den? jeg har engang hørt et ordsprog som lyder "hvis der er noget der er gratis, er det fordi du er varen" - dette udtryk passer meget godt her. Hvis vi som brugerer forventer at noget skal være gratis, må vi også forstå at det kommer med et "men". Dette "men" er, at vi får lov at bruge servicen og servicen får lov at holde øje med vores adfærd. 

