let kort;
let capture;
let hawaii;
let names = ['McLovin', 'Jørgensen', 'Andersen', 'Møller', 'Graa', 'Graasten'];
let button;

function setup() {
button = createButton('RANDOMIZE NAME');
button.position(50, 370);
button.mousePressed(randomName);
displayText = createP('');
displayText.position(135, 212);
displayText.style('font-size', '20px');
displayText.style('font-style', 'Arial');

//kortet
kort = createCanvas(500, 300);
kort.position(50, 50);

//video
capture = createCapture(VIDEO);
capture.hide();
}

function randomName() {
let index = floor(random(names.length));
let name = names[index];
displayText.html(name);
}


function draw() {
background(219, 215, 215);
image(capture, 15, 15, 150, 150 * capture.height / capture.width);


//HAWAII Tekst
textSize(55);
textFont("SILOM");
fill(0,0, 255);
text('HAWAII', 175, 65);
textSize(20);
text('DRIVER', 400, 40);
text('LICENSE', 400, 65);
fill(255, 0, 0);
text('ORGAN', 400, 260);
text('DONER', 400, 285);

textSize(20);
textFont("Arial");
fill(0, 0, 0);
text('NAME:', 15, 200);
text('YEAR OF BIRTH: 1999', 15, 230);


}
  