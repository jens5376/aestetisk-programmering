let x = 0;
let y = 0;
let spacing = 50;

function setup(){
    createCanvas(windowWidth, windowHeight);
    background(255);
}

function draw(){
    stroke(random(0, 255), random(0, 255), random(0, 255));
    if (random(1) < 0.20){
        line(x, y, x + spacing, y + spacing);
    } else if (random(1) > 0.40) {
        line(x, y + spacing, x + spacing, y);
    } else if (random(1) < 0.60) {
        line(x, y + 2/spacing, x + spacing, y + 2/spacing);
    } else if (random(1) < 0.80){
        line(x + 2/spacing, y, x + 2/spacing, y + spacing);
    } else {
        fill(random(0, 255), random(0, 255), random(0, 255));
        rect(x, y, spacing, spacing);
    }

    x = x + spacing;
    if (x > width){
        x = 0;
        y = y + spacing;
    }

}
