# Mini x - 5

[Open source](https://gitlab.com/jens5376/aestetisk-programmering/-/blob/main/MiniX%20-%205/minix5sketch.js)

[Kørende kode](https://jens5376.gitlab.io/aestetisk-programmering/MiniX%20-%205/index.html)

![](AUTO.png)

**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**


Jeg har taget en hel del inspiration fra "10 PRINT", men har forsøgt mig at give koden flere regler. Disse regler er baseret på hvor stor en chance der er, for hvor mellem 0 - 1, `random(1)` lander. I den oprindelige "10 PRINT" er det et binært udfald, altså er der 50% cance for / og 50% chance for \. Jeg delte koden op i 5 udfald, altså er der 20% cance for hver af udfaldne. Måden hvorpå jeg gjorde dette, var ved hjælp af `if`, `else if` og `else`. Jeg fik computeren til at vælge et decimal mellem 0 - 1, ved hjlælp af `random()` funktionen - afhængigt af hvor den landede, ville koden generer hhv. /, \, lordret linje, vandret linje eller en rektangel. 

Første jeg gjorde var at definere mine variabler, X, Y, og spacing. 

```
let x = 0;
let y = 0;
let spacing = 50;
```
Den eneste af varibalerne der reelt behøvede en værdi var spacing, da det er spacing der senere i koden ville definere størrelsesforholdene på de forskellige dele. Det vil gøre sig mere tydeligt hvordan dette fungerer senerer i teksten, da alt går tilbage til spacing og jeg på denne måde sikre mig at jeg kun skal rette dets værdi, for at ændre hele den underlæggende kode.  Jeg har også gjort meget brug af `random()`, ikke kun for at vælge hvilket print der skulle ske, men også hvilken farve printet skulle have. Farvekoden RGB løber fra 0 - 255, i alle tre faver. Derfor brugte jeg random funktionen til vælge en tilfældig værdi deriblandt `stroke(random(0, 255), random(0, 255), random(0, 255));`. På den måde får rød, grøn og blå en tilfældig farveværdi, der tilsammen sørger for at alle linjer har en forskellig farve.


Den hovedsagelige del af koden, ser sådan ud: 

```
function draw(){
    stroke(random(0, 255), random(0, 255), random(0, 255));
    if (random(1) < 0.20){
        line(x, y, x + spacing, y + spacing);
    } else if (random(1) < 0.40) {
        line(x, y + spacing, x + spacing, y);
    } else if (random(1) < 0.60) {
        line(x, y + 2/spacing, x + spacing, y + 2/spacing);
    } else if (random(1) < 0.80){
        line(x + 2/spacing, y, x + 2/spacing, y + spacing);
    } else {
        fill(random(0, 255), random(0, 255), random(0, 255));
        rect(x, y, spacing, spacing);
    }
```

Nu vil jeg gennemgå dem oppefra og ned. 

`random(1)` væger et decimal tal fra 0 - 1. 

Hvis tallet er mellem 0 og 0.20 printer koden /

Hvis tallet er mellem 0.20 og 0.40 printer koden \

Hvis tallet er mellem 0.40 og 0.60 printer koden en lodret linje 

Hvis tallet er mellem 0.60 og 0.80 printer koden en vandret linje

Hvis tallet bliver større end 0.80, printer koden en hel rektangel, med sider på størrelse med spacing. 



**What role do rules and processes have in your work?**

Man kan sige, at der er blevet sat et par regler for hvad der kan ske, men disse regler kun bliver sat i gang hvis tilfældigheden er gunstig. Her synes jeg der er spændende at overveje `random(1)`. Den er spændende fordi, vi reelt set ikke ved hvordan den fungerer. Det er en predefineret linje kode, som jeg antager er fuldstændigt fair men den tilgår P5.JS biblioteket, som jeg personligt ikke ved tilstrækkeligt om til at kunne forklare koden fyldstgørende. Udover det, synes jeg at `if` og `else if`, har spændende potentiale, da den gør koden mere levende eller uforudsigelig. Kode handler meget om at skabe, men hele ideen om at skabe noget der skaber sig selv er et spændende område. Kan kan sige at der er regler i kode i forvejen, f.eks. at koden loop'er i `function draw()` og sker en enkelt gang i `function setup()`, medmindre man aktivt skriver en linje kode, der omgår disse predefineret regler. Desto mere kode man lære at skrive, finder man ud af at næsten alt handler om regler og forståelse af dem. 

**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

Jeg synes det er svært at sige. Der er dele af mig der siger mig at regler i kode og autonomy delen, virker som to kontraster. Man sætter regler for hvad en kode skal gøre, men samtidigt er en af reglerne den skal følge at den selv kan bestemme. Hvad angår love-letter generatoren, synes jeg egentligt ikke det er vildt. Vi har de samme regler gældende i grammatik, som man så har defineret i en maskine. Jeg tror fremtiden byder på programmer, der sørger for at sætte sine egne regler, på godt og ondt. 

