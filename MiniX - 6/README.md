# Mini x - 6


[Få koden til at køre](https://jens5376.gitlab.io/aestetisk-programmering/MiniX%20-%206/index.html)

[Selve koden](https://gitlab.com/jens5376/aestetisk-programmering/-/blob/main/MiniX%20-%206/spilLogik.js)

![](flappy.png)


**Describe how does/do your game/game objects work?**


Jeg har en lavet to objekter, en "fugl" og "pipes". Fuglen kan kun forblive inden for kanvaset, den kan hoppe og den kan blive påvirket af "tyngdekraften". Det betyder altså den den falder til jorden, hvis ikke spilleren trykker på mellemrumstasten. Udover den, har jeg også lavet rektangler, som skal repræsentere pipes. Disse pipes bliver større og mindre tilfældigt og bevæger sig langs x aksen på kanvas. Det er ligesom de to hovedsagelige ting jeg har kodet. 

**Describe how you program the objects and their related attributes, and the methods in your game**

Jeg har fokuseret på tre ting, eller objekter, hhv. rørene / pipes, spilleren / bird og selve spil logikken. Objekt orienteret programmering har uden sammenligningen været den del af kodning jeg har haft sværest ved. Jeg har gjort et forsøg på at lave flappy bird, eller noget der minder om dette spil og set en masse youtube videoer for at få koden til at fungere. Først startede jeg med at definerer mine variabler i bird objektet. 
```
this.x = 50;
this.y = height/2;
this.gravity = 0.6;
this.lift = -15;
this.velocity = 0;
```
x og y placerer fuglen på kanvas, "velocity" styrer hvor hurtigt den falder eller stiger, "lift" og "velocity" styrer accelerationen af hoppet og hvor hurtigt fuglen skal falde igen.
```
this.up = function() {
      this.velocity += this.lift;
    }
```
`this.up` definerer hvad der skal ske, når fulgen skal hoppe. Den tilføjer lift til fulgens position og det ligner på den måde at den hopper. Den stiger altså 15, oveni hvor fulgens nuværende position på y-aksen er. Slutteligt sørgede jeg for at fuglen ikke kunne forlade kanvas, hverken gå over eller under:

```
  
    this.update = function() {
      this.velocity += this.gravity;
      this.y += this.velocity;
  
      if (this.y > height) {
        this.y = height;
        this.velocity = 0;
      }
  
      if (this.y < 0) {
        this.y = 0;
        this.velocity = 0;
      }
    }
```
Den kan altså ikke gå under 0, eller stige over højden. 

Efter jeg havde programmeret fuglen programmeret jeg pipes. Her var der en del ting jeg skulle sørge for, de skulle autogenere, de måtte ikke blive større end fuglen kunne komme igennem og højden på rørene skal ændre sig. Det sværeste var, at udseendet ikke kunne være statisk. Først defineret jeg igen objektets variabler

```
function Pipe() {
  this.x = width;
  this.top = random(height/2);
  this.bottom = random(height/2);
  this.w = 40;
  this.speed = 4;
```
Toppen og buden på pipes, viden er W og farten på pipes er selvfølgeligt speed. Så sørgede jeg for at at der altid blev dannet to samtidigt og at de gradvist flyttede sig.

```
  this.show = function() {
    fill(0, 205, 0);
    rect(this.x, 0, this.w, this.top);
    rect(this.x, height-this.bottom, this.w, this.bottom);
  }

  this.update = function() {
    this.x -= this.speed;
  }
```
Så alle de her pipes der spawner, flytter sig altså gradvist fra venstre mod højre med grundet speed. 

Slutteligt eksekveret jeg objekterne i min sketch, den jeg har valgt at kalde "spilLogik". Her vil jeg bare gennemgå de vigtigste dele.

Jeg søger for at fulgen hopper, når spilleren trykker på mellemrumstasten
```
function keyPressed() {
  if (key == ' ') {
    bird.up();
  }
}
```
Den kode jeg skrev ved fulgen, bliver altså her eksekveret når der bliver trykket på mellemrumstasten. 

I `function draw ()` indsætter jeg fuglen og pipes og sørger for, med en array, at der kontinuerligt bliver spawnet nye pipes med varierende størrelse. 

```
function draw() {
  background(0, 204, 255);

  for (var i = pipes.length-1; i >= 0; i--) {
    pipes[i].show();
    pipes[i].update();

    if (pipes[i].offscreen()) {
      pipes.splice(i, 1);
    }
  }

  
  bird.update();
  bird.show();

  if (frameCount % 100 == 0) {
    pipes.push(new Pipe());
  }
}
```

**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**

Jeg synes teksten vi læste, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge: Polity, 2017), siger mange af de samme ting som de tekster vi har læst i f.eks. software studies. Altså, at software gør mange ting nemmere, men selvom at det på overfalden bare kan virke som en hjælpende hånd, men grundet komplexitet af OOP kan den underliggende kode have præferencer og være partisk og på den måde være farlig overfor samfundet. 

**Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?**

Jeg synes det er svært at sammenligne det på den måde, at det er svært at forstå "abstraction" når man selv har skrevet koden og at man derfor ved, mere eller mindre, præcist hvad der forgår. Så vidt jeg kan forstå handler abstraktionen om at visse aspekter af kode i programmering bliver gemt væk eller hvertfald - men det er jo ikke noget nyt? Min pointe er, at alt i samfundet fungerer meget som OOP. Al matriale er er objekter og interagerer med hinanden, også mennesker imellem. Derimod synes jeg OOP har stort potentiale, da jeg forstiller mig at det er gennem OOP man laver simulationer, da man med simulationer skal tage højde for en masse kompliceret systemer som så skal opføre sig afhængigt af hinanden. 
