function Pipe() {
    this.x = width;
    this.top = random(height/2);
    this.bottom = random(height/2);
    this.w = 40;
    this.speed = 4;
  
    this.show = function() {
      fill(0, 205, 0);
      rect(this.x, 0, this.w, this.top);
      rect(this.x, height-this.bottom, this.w, this.bottom);
    }
  
    this.update = function() {
      this.x -= this.speed;
    }
  
    this.offscreen = function() {
      return (this.x < -this.w);
    }
  }