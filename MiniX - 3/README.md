# Mini X - 3

[Link til opensource](https://gitlab.com/jens5376/aestetisk-programmering/-/blob/main/MiniX%20-%203/minix3sketch.js)

[Kørende kode](https://jens5376.gitlab.io//aestetisk-programmering/MiniX%20-%203/index.html)


![](solen.png)!



**Describe your throbber design, both conceptually and technically**

Jeg har lavet en throbber, der ligner solen, hvor throbber der kører rundt i ring symbolisere solens stråler der går ud i universet. Udover den throbber, har jeg teknisk set også lavet en anden, som er tekst der kører rundt i ring, hvor der står "4.6 bil. years and counting", som altså er for at sætte fokus på at selv solen har en alder - noget jeg også vil komme ind på senere. 

Den første ting, som er lidt specifikt for at lave en annimation som denne, er at jeg har startet med at definere framecount.
```
function setup() {
createCanvas(windowWidth, windowHeight);
frameRate (60);
}
```
Grunden til at jeg definere framerate, skyldes at jeg senere skal definere hvor hurtigt der skal tegnes en ny "stråle", så man på den måde bestemmer farten på throbberen. Foruden det, har jeg brugt meget `push();` og `pop();`, for at indkaplse forskellige linjer af kode så de ikke relatere sig til den resterende del af koden. De linjer af kode, der f.eks. skulle indkapsles, er `translate();`, som hvis ikke indkapsles, akummuleres løbende i koden hvis man gør brug af den flere gange, med flere værdier. Jeg har også brugt en del variabler, som jeg har defineret med `let();`, så jeg ikke skal skrive den samme værdi flere gange i koden. 


Måden hvorpå jeg har fået min `ellipse();` til at kører i ring, har meget at gøre med, at koden placerer en ny ellipse en for hver en frame, den løber igennem. Grunden til at ellipsen ændre plads hver gang, er at den ligger i `function draw();` og dette gør, at i et loop, bliver placeret en ny ellipse. Farten af ellipsen afhænger af frameraten og placeringen afhænger af regnestykket i X- og Y-aksen's sted i syntaksen.

```
 function drawElements() {
   let num = 30;
   let vinkel = 0 ;
   let minusVinkel = 0;
   push();
   translate(width/2, height/2);
   let cir = 360/num*(frameCount%num);
   rotate(radians(cir));
   noStroke();
   ellipseMode(CENTER);
   fill(255, 200, 0);
   ellipse(windowWidth/20, windowHeight/20, 300, 40);
   pop();
```

**What do you want to explore or express?**


Mit hovedsagelige punkt og udforskelsesområde er menneskets opfattelse af tid, og hvor vores forståelse stammer fra - altså solen. Derudover ville jeg også undersøge og understrege hvor kort tid mennesket og menneskets definition af tid har eksisteret. Hvis vi gør det op i år, har solen eksiteret i 4,6 milliarder år, hvoraf vi opfandt vores tid defineret for få årtusinder siden. Jeg synes det er facinerende og lidt angstprovokerende, hvor længe universet, solensystemet og dets lige har eksisteret sammenlignet med hvor længe vi har talt sekunder, minutter, timer, dage og år. Udover at vi har opfundet vores definition af tid har menneskeheden også senere opdaget, at tid er relativt. Ikke i den forstad, at tid går langesomere når man keder sig, men i den forstand at tyngdekraften og fart har indvirkning på temporalitet. 



**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?**

Den hovedsagelige tidsrelateret syntakst jeg har gjort brug af, er framecount i form af `let cir = 360/num*(frameCount%num)` som er hvor mange gange skærmen indlæser noget nyt. En direkte oversættelse ville være "ramme tæller". Faktum er, at hvis man tæller noget, så er der de facto noget der sker kontinuerligt og indikere også derfor at der er noget der sker over tid. Selvom computeren systmet teknisk set tæller i en evighed, hvis man sætter den til det, vil den have en ende hvis jeg lukker programmet. 



**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**

Personligt synes jeg, at et af de mest interessante throbbere der findes, er microsofts tidelige throbber, som sådanset ikke er en animation, men blot af musen på skærmen bliver omdannet til et timeglas. Jeg synes denne er interssant, fordi Microsoft og Macintosh skulle finde den bedst mulige måde at kommunikere til brugeren at vedkommende skulle vente. Jeg synes dette er interssant fordi de er så simpel og nem måde at kommunikere tid på, selvom det egentligt handler om at computeren arbejder. Det facinerende ved computeret tid, menneskets opfattelse af tid og tid i fysikkens verden, er tre vidt forskellige ting, som samtidigt er det samme og afhængeige af hinanden på tværs. 



