


function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate (60);
 }
 
 function draw() {
   background(10, 15);
   drawElements();
 }
   //solen stråle / trobber
   function drawElements() {
   let num = 30;
   let vinkel = 0 ;
   let minusVinkel = 0;
   push();
   translate(width/2, height/2);
   let cir = 360/num*(frameCount%num);
   rotate(radians(cir));
   noStroke();
   ellipseMode(CENTER);
   fill(255, 200, 0);
   ellipse(windowWidth/20, windowHeight/20, 300, 40);
   pop();

   //solen
   noStroke();
   fill(255,170, 0);
   ellipse(windowWidth/2, windowHeight/2, 200, 200);
   noStroke();
   fill(255,150, 0);
   ellipse(windowWidth/2, windowHeight/2, 170, 170);

   // jorden
   noStroke();
   fill(0,100,255);
   circle(200, 200, 80, 80);
   noStroke();
   fill(0, 255, 30);
   ellipse(189, 188, 30, 45);
   noStroke();
   fill(0, 255, 30);
   ellipse(200, 183, 30, 35);
   noStroke();
   fill(0, 255, 30);
   ellipse(185, 212, 20, 25);
   noStroke();
   fill(0, 255, 30);
   ellipse(220, 210, 15, 30);

   //tekst throbber
   textSize(30);
   textFont("Castellar");
   textAlign(CENTER,CENTER);
   fill(255, 255, 255);
   text('4,6 bil. years and counting',400 * sin(frameCount / (120)) + width / 2, 400 * cos(frameCount / (120)) + height / 2);
   
   
 }

  
  










